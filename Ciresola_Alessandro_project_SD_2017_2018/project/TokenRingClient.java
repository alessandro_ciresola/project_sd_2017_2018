package project; 
import java.net.*; 
import java.io.*; 

/**
*Classe Client per il testing di reti costruite in architettura Token Ring 
*/
public class TokenRingClient 
{   
  public static void main(String[] args) 
  {
    Socket soc = null; 
    BufferedReader in = null;  
    ObjectOutputStream oos = null; 
    ObjectInputStream ois = null; 
       
    try
    {
      soc = new Socket("localhost", 1234); 
      oos = new ObjectOutputStream(soc.getOutputStream()); 
      ois = new ObjectInputStream(soc.getInputStream()); 
      in = new BufferedReader(new InputStreamReader(System.in));
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
    }
        
    String readLine = ""; 
       
    while(!readLine.equals("q"))
    {
      System.out.println("Premere q per terminare..."); 
      printOptions(); 
      System.out.print("Comando da eseguire?> ");
      try
      {
        readLine = in.readLine(); 
        oos.writeObject(readLine); 
        oos.flush();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace(); 
      }
    }
        
    //Nel momento in cui viene premuto q per terminare  
    try
    {
      in.close(); 
      ois.close(); 
      oos.close(); 
      soc.close();
    }
    catch(IOException ioe2)
    {
      ioe2.printStackTrace(); 
    }  
  }//[m]main	

  public static void printOptions()
  {
    System.out.println("Per creare un nuovo nodo:"); 
    System.out.println("createNode group_name port_number node_name"); 
    System.out.println("Per distruggere un nodo creato:"); 
    System.out.println("destroy node_name");
  }//[m]printOptions
}//{c}TokenRingClient
