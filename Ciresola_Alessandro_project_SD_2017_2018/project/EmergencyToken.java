package project; 

public class EmergencyToken implements java.io.Serializable 
{
  private String from; 
  private String to; 
     
  public EmergencyToken(String from, String to)
  {
    this.from = from; 
    this.to = to; 
  }

  /**
  *@return from nome del nodo a cui sto chiedendo di generare e di inviarmi il token di emergenza 
  */
  public String getFrom()
  {
    return from; 
  }

  /**
  *@return to nome del nodo che rappresenta il next node del nodo a cui sto chiedendo di inviarmi il token di emergenza 
  */
  public String getTo()
  {
  	return to; 
  }
}//{c}EmergencyToken
