package project; 

import java.util.ArrayList; 

public class MulticastMessage implements java.io.Serializable
{    
    private int version = 0; 
    //source contiene il nome del nodo che ha generato il messaggio, se ricevo un messaggio e il nodo sorgente di tale messaggio e' caduto 
    //allora elimino il messaggio 
    private String source; 
    //lista di destinatari 
    private ArrayList<String> destination; 
    //messaggio da inviare
    private String message; 
        
    public MulticastMessage(String source, ArrayList<String> destination, String message)
    {
      this.source = source; 
      this.destination = destination; 
      this.message = message; 
    } 

    public MulticastMessage(int version, String source, ArrayList<String> destination, String message)
    {
      this.version = version; 
      this.source = source; 
      this.destination = destination; 
      this.message = message; 
    }

    /**
    *Il multicast message deve essere convertito in un array di byte prima di essere trasmesso sul ring. 
    *@return byteMessage : MulticastMessage convertito in un array di byte 
    */
    public byte[] toByteArray()
    {
      String destins = ""; 
      for(String dest : destination)
      {
      	destins = ( destins + dest + " ");   
      }
       
      String codedMessage = ("version " + version + " source " + source + " destination " + destins + "message " + message); 	
      byte[] byteMessage = codedMessage.getBytes(); 

      return byteMessage; 
    }//[m]toByteArray
        
    /**
    *@return source nome del nodo che ha generato il messaggio 
    */    
    public String getSource()
    {
      return source; 
    }//[m]getSource
        
    /**
    *@return destination lista dei nomi dei nodi ai quali inviare il messaggio 
    */
    public ArrayList<String> getDestination()
    {
      return destination; 
    }//[m]getDestination
     
    /**
    *Incrementa il numero di versione del MulticastMessage attuale 
    */
    public void incrementVersion()
    {
      version++; 
    }//[m]incrementVersion

    /**
    *@return version numero di versione del MulticastMessage attuale 
    */
    public int getVersion()
    {
      return version; 
    }//[m]getVersion

    /**
    *@return message messaggio contenuto nel multicast message 
    */
    public String readMessage()
    { 
      return message; 
    }//[m]readMessage
}//{c}MultiCastMessage
