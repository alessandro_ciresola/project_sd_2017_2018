package project; 

//Classe che rappresenta il token che viene scambiato tra i nodi appartenenti allo stesso ring 
public class Token implements java.io.Serializable
{
  private String name = null; 
  private final String DEFAULT_NAME = "Token";  

  public Token()
  {
     name = DEFAULT_NAME; 
  }

  public Token(String name)
  {
     this.name = name; 
  }

  public String getName()
  {
     return name; 
  }
}//{c}Token
