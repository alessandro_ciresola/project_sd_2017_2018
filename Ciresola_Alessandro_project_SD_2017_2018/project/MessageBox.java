package project; 

import java.util.ArrayList; 

public class MessageBox implements java.io.Serializable 
{
    private boolean isReadd; 
    private ArrayList<MulticastMessage> boxMulticast = null;  
    private ArrayList<byte[]> box = null; 
    private String owner = null; 

    public MessageBox(String owner, boolean isAMulticastBox) 
    {
      isReadd = false;  
      this.owner = owner; 

      if(isAMulticastBox)
      {
        boxMulticast = new ArrayList<MulticastMessage>(1); 
      }
      else
      {
	box = new ArrayList<byte[]>(1); 
      }
    }

    /**
    *Metodo che ritorna true se il MessageBox e' stato letto, false in caso contrario 
    *@return isReadd 
    */
    public boolean isRead()
    {
      return isReadd; 
    }//[m]isRead

    /**
    *@return owner nome del nodo possessore di questo MessageBox 
    */
    public String getOwner()
    {
      return owner; 
    }//[m]getOwner

    /**
    *Metodo che setta al valore desiderato il parametro isReadd 
    *@param value valore a cui si vuole settare isReadd
    */
    public void setValue(boolean value)
    {
      isReadd = value; 
    }//[m]setValue

    /**
    *@return box ArrayList che contiene i messaggi sotto forma di array di byte 
    */
    public ArrayList<byte[]> getBox()
    { 
      return box; //null se il MessageBox attuale contiene MulticastMessage e non byte[] 
    }//[m]getBox

    /**
    *@return boxMulticast ArrayList che contiene i messaggi sotto forma di oggetti della classe MulticastMessage  
    */
    public ArrayList<MulticastMessage> getMulticastBox()
    {
      return boxMulticast; //null se il MessageBox attuale contiene byte[] e non MulticastMessage 
    }//[m]getMulticastBox
    
    /**
    *Metodo che inserisce un messaggio rappresentato come un array di byte all'interno di box, l'inserimento fallisce 
    * se box e' un puntatore a null 
    *@param mx messaggio rappresentato come un array di byte da inserire in box 
    */
    public void addMessage(byte[] mx)
    {
      if(box != null)
      {	
	box.add(0, mx); 
      }
      else
      {
	System.out.println("Il messaggio non puo' essere inserito in quanto box e' un puntatore a null!!!"); 
      }
    }//[m]addMessage
    
    /**
    *Metodo che inserisce un messaggio rappresentato come un oggetto della classe MulticastMessage all'interno di boxMulticast, 
    * l'inserimento fallisce nel caso in cui boxMulticast sia un puntatore a null 
    *@param mx messaggio visto come oggetto della classe MulticastMessage da inserire in boxMulticast 
    */
    public void addMMessage(MulticastMessage mx)
    {
      if(boxMulticast != null)
      {	
        boxMulticast.add(0, mx); 
      }
      else
      {
	System.out.println("Il messaggio non puo' essere inserito in quanto boxMulticast e' un puntatore a null!!!"); 
      }
    }//[m]addMMessage
    
    /**
    *Metodo che rimuove un messaggio visto come un array di byte da box, la rimozione restituisce null nel caso in cui 
    * box sia un puntatore a null 
    *@return Il primo messaggio contenuto in box se box non punta a null, in caso contrario viene restituito null 
    */
    public byte[] removeMessage()
    {
      if(box != null)
      {
        return box.remove(0);
      }  
      else
      {
	return null; 
      }
    }//[m]removeMessage
    
    /**
    *Metodo che rimuove un messaggio visto come un oggetto della classe MulticastMessage da boxMulticast,
    * la rimozione restituisce null nel caso in cui boxMulticast sia un puntatore a null 
    *@return Il primo messaggio contenuto in box se box non punta a null, in caso contrario viene restituito null 
    */
    public MulticastMessage removeMMessage() 
    {
      if(boxMulticast != null)
      {
	return boxMulticast.remove(0);
      }  
      else
      {
	return null; 
      }
    }//[m]removeMMessage
}//{c}MessageBox 
