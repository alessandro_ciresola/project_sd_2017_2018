package project; 

import java.rmi.*; 
import java.rmi.registry.*; 

public interface ServerNodeRegistryInterface extends Remote
{
   int registryGroup(String groupName) throws RemoteException; 
}
