package project; 

import java.rmi.*; 

public class ConnectionRequest implements java.io.Serializable
{
  private boolean isReceived; 
  private NodeInterface requestNode; 
       
  public ConnectionRequest(NodeInterface requestNode)
  {
    this.requestNode = requestNode; 
    isReceived = false; 
  }
         
  /**
  *@return requestNode nodo che ha presentato la richiesta di connessione
  */
  public NodeInterface getNode()
  {
    return requestNode; 
  }//[m]getNode
      
  /**
  *@return isReceived: a true se la richiesta di connessione presentata e' stata accolta, a false in caso contrario 
  */
  public boolean requestSatisfied()
  {
    return isReceived; 
  }//[m]requestSatisfied
       
  /**
  *Metodo per settare il parametro isReceived a true 
  */
  public void setToReceived()
  {
    isReceived = true; 
  }//[m]setToReceived
}//{c}ConnectionRequest
