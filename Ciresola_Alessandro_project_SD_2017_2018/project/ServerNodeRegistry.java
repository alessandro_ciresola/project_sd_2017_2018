package project; 

import java.rmi.*; 
import java.rmi.registry.*; 
import java.rmi.server.UnicastRemoteObject; 
import java.io.*; 
import java.util.*; 

public class ServerNodeRegistry implements ServerNodeRegistryInterface, Serializable 
{
	private static Hashtable<String, Integer> listGroups = new Hashtable<String, Integer>(); 
	private static int counterPort = 0; 
	private static int initPort = 2200; 

	/**
	*Metodo per la creazione del registry RMI associato al gruppo groupName. Tale metodo restituisce il port su cui 
	* e' stato eseguito l'export del registry. 
	*@param groupName nome del gruppo per il quale deve essere creato un nuovo registry se non presente nella tabella hash, se gia' presente 
	* viene solo restituito il port senza la creazione del registry 
	*@return retval numero del port su cui e' stato esportato il registry associato a groupName 
	*/
	public int registryGroup(String groupName) 
	{
	   //Registry retval = null; 
	   Registry rmiRegistry = null; 
	   int retval = -1; 

	   synchronized(listGroups)
	   {
	   	  if(listGroups.containsKey(groupName))
	   	  {
	   	  	retval = listGroups.get(groupName).intValue(); 
	   	  }
	   	  else 
	   	  {
	   	  	boolean flag = false; 

	   	  	while(!flag)
	   	  	{
	   	   	  try
	   	  	  {
	   	  	    rmiRegistry = LocateRegistry.createRegistry(initPort + counterPort);
	   	  	    retval = (initPort + counterPort); 
	   	  	    System.out.println("Gruppo registrato su porta " + retval); 
	   	  	    flag = true; 
	   	  	  }
	   	  	  catch(RemoteException re)
	   	  	  {
	   	  	  	System.out.println("Cambio di porta..."); 
	   	  	  	counterPort++; 
	   	  	  }
	   	  	}

	   	  	counterPort++; 
	   	  	listGroups.put(groupName, Integer.valueOf(retval));
	   	  }
	   }
	   return retval; 
	}//[m]registryGroup
}//{c}ServerNodeRegistry