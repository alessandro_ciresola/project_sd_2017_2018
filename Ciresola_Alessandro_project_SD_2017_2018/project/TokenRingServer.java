package project; 
import java.net.*; 
import java.io.*; 
import java.util.*; 
import java.rmi.*; 

/**
*Classe Server per il testing di reti costruite in architettura Token Ring
*/
public class TokenRingServer 
{
  public static void main(String[] args) 
  {
    ServerSocket serverSocket = null; 
    Socket soc = null; 
    ObjectInputStream ois = null;  
    ObjectOutputStream oos = null;
    //Salvo le coppie (nome_nodo, riferimento all'oggetto) --> Serve nel momento in cui si vuole eseguire un destroy di un nodo!!
    HashMap<String, Node> hashmap = new HashMap<String, Node>(); 
   	  
    try
    {
      serverSocket = new ServerSocket(1234);
      soc = serverSocket.accept();
      oos = new ObjectOutputStream(soc.getOutputStream());
      ois = new ObjectInputStream(soc.getInputStream());  
    }
    catch(IOException ie)
    {
      ie.printStackTrace(); 
    }

    Scanner sc = null;  
    String lineRead = null; 
    try
    {   
      lineRead = (String)(ois.readObject());   
    }
    catch(IOException|ClassNotFoundException ioe)
    {
    	ioe.printStackTrace(); 
    }

    sc = new Scanner(lineRead); 
    String firstToken = sc.next(); 

    while(!firstToken.equals("q"))
    {
      if(firstToken.equals("createNode"))
      {
   	//Estraggo gli altri parametri dalla stringa appena letta 
        String groupName = sc.next(); 
        int portNumber = sc.nextInt(); 
        String nodeName = sc.next(); 
        
        // Creo il nodo richiesto se non e' gia' stato creato in precedenza... 
        if(!(hashmap.containsKey(nodeName)))
        {
          Node node = new Node(groupName, portNumber, nodeName);
          hashmap.put(nodeName, node);
        }
        else
        {
          System.out.println("Non e' possibile creare il nodo in quanto gia' presente!!");
        } 
      }
      else if(firstToken.equals("destroy"))
      {
   	//Estraggo il nome del nodo da distruggere
   	String nodeToDestroy = sc.next(); 

   	if(hashmap.containsKey(nodeToDestroy))
   	{
   	  Node nd = hashmap.remove(nodeToDestroy); 
   	  try
   	  {
   	    nd.destroy(); 
   	  }
   	  catch(RemoteException re)
   	  {
   	    re.printStackTrace(); 
   	  }
     	}
  	else
   	{
   	  System.out.println("Il nodo che si vuole distruggere non esiste!");
   	}
      }
      else
      {
   	System.out.println("Comando errato riprovare!"); 
      }
        
      try
      {
        lineRead = (String)(ois.readObject()); 
      }
      catch(IOException|ClassNotFoundException ixe)
      {
      	ixe.printStackTrace(); 
      }

      sc = new Scanner(lineRead); 
      firstToken = sc.next();
   }//endwhile

    //Se viene premuto q...
   try
   { 
     ois.close();
     oos.close(); 
     sc.close(); 
     soc.close(); 
   }   
   catch(IOException ioe)
   {
     ioe.printStackTrace(); 
   }
  }//[m]main
}//{c}TokenRingServer
