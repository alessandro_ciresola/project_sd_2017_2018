package project; 

import java.rmi.*; 
import java.rmi.registry.*; 
import java.rmi.server.UnicastRemoteObject; 
import java.io.*; 
import java.util.*; 
import java.util.concurrent.locks.*; 

public class Node implements NodeInterface, Serializable, Runnable
{
  private static ServerNodeRegistry servTmp = new ServerNodeRegistry(); 
  private static ServerNodeRegistryInterface server = null; 
  //RMI registry associato al gruppo a cui appartengo 
  private Registry myGroupRegistry = null; 
  //Riferimento al nodo successivo nel ring 
  private NodeInterface nextNode = null; 
  //Nome del gruppo a cui appartengo 
  private String myGroup = null; 
  //Nome del nodo attuale 
  private String nodeName = null; 
  //Porta sulla quale e' collocato l'RMI registry associato al gruppo a cui appartengo 
  private int registryPort = 0; 
  //Container che servono in fase di invio e di ricezione del token 
  private Container container = new Container(); 
  private Container internalContainer = new Container(); 
  //Se thisNodeAlive e' a false allora il nodo attuale e' caduto 
  private boolean thisNodeAlive; 
  private ReentrantLock lck = new ReentrantLock(true); 
  private List<EmergencyToken> emergencyList = new ArrayList<EmergencyToken>(); 

  //Per evitare che i nomi dei gruppi siano direttamente visibili a tutti i nodi e quindi un nodo potrebbe prelevare un nome
  //di un altro gruppo, ottenere il port su cui e' collocato il registry per tale gruppo e interagire con tale gruppo 
  //metto una tabella hash di <Integer, List<ConnectionRequest>> dove l'Integer è il codice hash del nome del gruppo  
  //La lista di ConnectionRequest e' una lista di nodi che sono in attesa di essere aggiunti al gruppo  
  private static Hashtable<Integer, List<ConnectionRequest>> joinQueue = new Hashtable<Integer, List<ConnectionRequest>>();  
  //Per ogni gruppo vi e' una stringa che contiene il nome del nodo che attualmente possiede il token 
  private static Hashtable<Integer, String> tokenOwners = new Hashtable<Integer, String>(); 
  //Ad ogni gruppo e' associato un read write lock per accedere e modificare l'RMI registry di tale gruppo 
  private static Hashtable<Integer, ReentrantReadWriteLock> locks = new Hashtable<Integer, ReentrantReadWriteLock>(); 
  //MessageBox che servono per l'invio e la ricezione di messaggi 
  private MessageBox receiveContainer = null; 
  private MessageBox sendContainer = null; 
  private MessageBox msendContainer = null; 
  //stub necessario per il binding su mygroupregistry 
  private Remote stub = null; 
   
  // Costruttore 
  public Node(String group, int exportPort, String nodeName)
  {
    myGroup = group; 
    this.nodeName = nodeName; 
    List<ConnectionRequest> list = null; 
    thisNodeAlive = true; 
    //receiveContainer contiene messaggi sottoforma di array di byte 
    receiveContainer = new MessageBox(nodeName, false);  
    //sendContainer e msendContainer contengono messaggi visti come oggetti della classe MulticastMessage 
    sendContainer = new MessageBox(nodeName, true); 
    msendContainer = new MessageBox(nodeName, true); 
    ReentrantReadWriteLock lck = null; 

    //Metto il synchronized per rendere atomica la sequenza di operazioni 
    synchronized(joinQueue)
    {
      if(!joinQueue.containsKey(Integer.valueOf(myGroup.hashCode())))
      {
        list = new ArrayList<ConnectionRequest>(); 
        joinQueue.put(Integer.valueOf(myGroup.hashCode()), list); 
      }
      else
      {
        list = joinQueue.get(Integer.valueOf(myGroup.hashCode())); 
      }
    }

    synchronized(tokenOwners)
    {
      if(!tokenOwners.containsKey(Integer.valueOf(myGroup.hashCode())))
      {
        tokenOwners.put(Integer.valueOf(myGroup.hashCode()), new String(""));
      }
    }

    synchronized(locks)
    {
      if(!locks.containsKey(Integer.valueOf(myGroup.hashCode())))
      {
        lck = new ReentrantReadWriteLock(); 
        locks.put(Integer.valueOf(myGroup.hashCode()), lck);
      }
      else
      {
        lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
      }
    }

    //Server di gestione dei port dei registry per ogni gruppo
    try
    {
      if(server == null)
      {
        server = (ServerNodeRegistryInterface)UnicastRemoteObject.exportObject(servTmp, 2346);
      }

      registryPort = server.registryGroup(group);
      myGroupRegistry = LocateRegistry.getRegistry(registryPort);
      stub = UnicastRemoteObject.exportObject(this, exportPort); 
      
      /*
      Non viene eseguito subito il binding sul registry myGroupRegistry, in quanto il nodo attuale viene messo in coda(tramite il metodo join) per 
      accedere al gruppo che gli e' stato assegnato e quindi attualmente non e' ancora entrato a far parte del gruppo. 
      Nel registry RMI associato a ciascun gruppo vengono salvati solo i nodi che fanno parte del token ring, se sono nella coda non sono ancora 
      stati registrati sul registry! 
      */

      //Una volta inizializzato il nuovo nodo lo connetto al 
      //gruppo che gli e' stato assegnato in fase di costruzione
      join(); 
    }
    catch(RemoteException rex)
    {
      rex.printStackTrace(); 
    }
  }

  /**
  *Nel momento in cui viene creato un nuovo nodo, tale nodo controlla quanti sono gli iscritti al gruppo a cui e' stato assegnato:
  * - Se nel registry RMI del gruppo a cui e' stato assegnato non vi sono nodi significa che il gruppo non e' ancora stato creato, di conseguenza
  * il nodo si registra sull'RMI registry, crea un token e comincia a farlo circolare 
  * - Se nel registry RMI del gruppo a cui e' stato assegnato vi sono gia' dei nodi allora il nodo attuale crea una ConnectionRequest e la inserisce 
  * nella coda relativa al gruppo che gli e' stato assegnato 
  */
  public void join() throws RemoteException
  {
    Token token = null; 
    List<ConnectionRequest> tmpList = null; 
    ReentrantReadWriteLock lck = null; 
    ConnectionRequest connReq = null; 
    
    //Hashtable gia' synchronized  
    tmpList = joinQueue.get(Integer.valueOf(myGroup.hashCode())); 
    lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
    
    synchronized(tmpList) 
    {
      //Se sono il primo nodo iscritto al gruppo creo un nuovo gruppo con solo me stesso all'interno genero un token, lo invio e attendo in ricezione
      lck.readLock().lock(); 
      if(myGroupRegistry.list().length == 0) // Attualmente non ci sono iscritti al gruppo, mi iscrivo e creo il gruppo... 
      {
        lck.readLock().unlock(); 
          
        //"creo" il gruppo
    	System.out.println(this.getName() + " ha creato un nuovo gruppo!!!!");
          
        lck.writeLock().lock();
          myGroupRegistry.rebind(nodeName, stub); 
    	lck.writeLock().unlock(); 
        //Setto me stesso come nextNode in quanto sono l'unico partecipante del gruppo 
        nextNode = this; 
        //Creo il token che verra' poi scambiato tra i membri del token ring 
    	token = new Token(); 
        //Inizio a far circolare il token 
    	nextNode.sendToken(token);
    	System.out.println(this.getName() + " ha trasmesso un token a " + nextNode.getName() + " !!!!");
      }
      //Se non sono il primo nodo iscritto al gruppo posto una nuova richiesta di connessione al gruppo in cui voglio entrare
      //e mi siedo su tale richiesta in attesa di essere risvegliato... 
      else 
      {
        lck.readLock().unlock(); 
    	System.out.println(this.getName() + " ha postato una richiesta di connessione al gruppo "+ this.myGroup + " !!!!");
    	//Creo una nuova richiesta di connessione 
    	connReq = new ConnectionRequest(this); 
    	//inserisco la richiesta nella lista 
    	tmpList.add(connReq); 
      }
    }
    
    /*
    Se in questo istante un nodo del ring riuscisse a prendere il lock su tmpList, eseguire un remove della connectionRequest 
    postata e fare un notify su tale richiesta di connessione non vi sarebbero problemi in quanto il parametro isReceived 
    verrebbe settato a true ed in tal caso connReq.requestSatisfied fornirebbe true come risultato.   
    */

    if(connReq != null) // Non e' un puntatore a null! Se puntasse a null avrei semplicemente creato un nuovo gruppo!!
    {
      synchronized(connReq)
      {
    	if(!connReq.requestSatisfied())
    	{
    	  try
    	  {
    	    connReq.wait(); 
    	  }
    	  catch(InterruptedException ie)
    	  {
    	    ie.printStackTrace(); 
    	  }
  	}
      }
    }
    
    (new Thread(this)).start(); 
    (new Thread(this.new TokenReceiver())).start(); 
    (new Thread(this.new TokenOwnerMonitor())).start(); 
    (new Thread(this.new MessageReceiver())).start(); 
    (new Thread(this.new MessageSender())).start(); 
  }//[m]join 

  /**
  *Restituisce un riferimento all'RMI registry del gruppo di cui il nodo attuale fa parte
  *@return myGroupRegistry: riferimento all'RMI registry associato al gruppo/ring a cui appartengo   
  */
  public Registry getRegistry() throws RemoteException
  {
    return myGroupRegistry; 
  }//[m]getRegistry

  /**
  *Metodo che viene utilizzato nel momento in cui si deve eseguire il binding di un nodo che e' stato inserito in coda . 
  *@return stub : Stub associato al nodo attuale creato in fase di export del nodo attuale sulla porta assegnata in fase di costruzione 
  */
  public Remote getStub() throws RemoteException
  {
    return stub; 
  }

  /**
  *Metodo che restituisce un riferimento al receiveContainer del nodo attuale. 
  *@return receiveContainer Container per la ricezione dei messaggi associato al nodo attuale 
  */
  public MessageBox getReceiveContainer() throws RemoteException
  {
    return receiveContainer; 
  }

  /**
  *Metodo che calcola un valore intero casuale compreso tra minVal e maxVal. 
  *@param minVal minimo valore intero dell'intervallo 
  *@param maxVal massimo valore intero dell'intervallo 
  */
  public int randVal(int minVal, int maxVal) throws RemoteException
  { 
    int diff = maxVal-minVal;
    return (int)(Math.random()*((double)diff+1.0d))+minVal;
  }

  /**
  *Metodo che acquisisce il lock su container, imposta il parametro isRead a false ed inserisce il token in tale container. 
  *@param tok: Token che si vuole inviare al nodo successivo del ring 
  */
  public void sendToken(Token tok) throws RemoteException
  {
    synchronized(container)
    {
      container.setValue(false); 
      container.getList().add(0, tok); 
    }
  }//[m]sendToken 

  /**
  *Metodo per la trasmissione di un messaggio in multicast ad un insieme predefinito di nodi del ring a cui appartengo. 
  * Il messaggio da trasmettere e' rappresentato come un array di byte. 
  *@param sms : messaggio che deve essere inviato in multicast 
  */
  public void sendMessage(byte[] sms) throws RemoteException
  {
    synchronized(receiveContainer)
    {
      receiveContainer.setValue(false); 
      receiveContainer.getBox().add(0, sms); 
    }
  }//[m]sendMessage

  /**
  *Metodo per impostare il nodo che segue il nodo attuale all'interno del token ring. 
  *@param nextNode nodo successivo al nodo attuale 
  */
  public void setNextNode(NodeInterface nextNode) throws RemoteException
  {
    this.nextNode = nextNode;  
  }//[m]setNextNode

  /**
  *Metodo che restituisce il nome del nodo attuale. 
  *@return nodeName: nome del nodo attuale 
  */
  public String getName() throws RemoteException
  {
    return nodeName; 
  }//[m]getName

  /**
  *Metodo per simulare la caduta di un nodo nel ring. Supponiamo che nel momento in cui un nodo viene distrutto esso non risponda 
  * più ai messaggi di check inviati e venga eseguito un unbind dall'rmi registry del gruppo a cui era stato assegnato. 
  */
  public void destroy() throws RemoteException
  {
    List<ConnectionRequest> tempList = null; 
    ReentrantReadWriteLock lck = null; 
    System.out.println("Caduto il nodo: " + nodeName + " !!!!");

    try
    {
      lck = locks.get(Integer.valueOf(myGroup.hashCode()));  
      lck.writeLock().lock(); 
        myGroupRegistry.unbind(nodeName); 
      lck.writeLock().unlock(); 
      //Non ricevo piu' correttamente i messaggi ed i token che mi vengono inviati....
      thisNodeAlive = false;
    }
    catch(NotBoundException nbe)
    {
      nbe.printStackTrace(); 
    }
  }//[m]destroy
  
  /**
  *Metodo che restituisce il container del nodo attuale 
  *@return container del nodo attuale 
  */
  public Container getContainer() throws RemoteException
  {
    return container; 
  }//[m]getContainer

  /**
  *Metodo sincronizzato con internalContainer per l'aggiunta di un token. 
  *@param token oggetto della class Token da inserire all'interno di internalContainer 
  */
  public void sendToInternalContainer(Token token) throws RemoteException
  {
    synchronized(internalContainer)
    {
      internalContainer.getList().add(token); 
    }
  }//[m]sendToInternalContainer

  /**
  *Metodo per la conversione di un array di byte in un MulticastMessage 
  *@param byteMex messaggio da convertire in un MulticastMessage 
  */
  public MulticastMessage fromByteToMulticast(byte[] byteMex) throws RemoteException
  {
    String convMex = new String(byteMex); 
    Scanner sc = new Scanner(convMex); 
    int version = 0; 
    String source = null; 
    ArrayList<String> destin = new ArrayList<String>(); 
    String message = ""; 
    //("version " + version + " source " + source + " destination " + destins + "message " + message); 
    //Estraggo il numero di versione 
    if(sc.next().equals("version"))
    {
      version = sc.nextInt(); 
    }  
    else
    {
      System.out.println("Errore, impossibile leggere il messaggio!"); 
    }
   
    if(sc.next().equals("source"))
    {
      source = sc.next(); 
    }  
    else
    {
      System.out.println("Errore, impossibile leggere il messaggio!"); 
    }
    
    if(sc.next().equals("destination"))
    {
      boolean flag = true; 
      while(flag)
      {
        String firstDest = sc.next(); 
        if(!firstDest.equals("message")) 
        {
          destin.add(firstDest); 
        }
        else
        {
          flag = false; 
        }
      }
    }  
    else
    {
      System.out.println("Errore, impossibile leggere il messaggio!"); 
    }
     
    //Leggo il messaggio 
    while(sc.hasNext())
    {
      message = (message + sc.next() + " "); 
    }

    MulticastMessage mmex = new MulticastMessage(version, source, destin, message);
    
    return mmex; 
  }//[m]fromByteToMulticast

  /**
  *Metodo che crea e restituisce l'emergency token associato al nodo attuale  
  *@return emtoken EmergencyToken che viene fornito come risposta al nodo che sta cercando di ricostruire la rete
  */
  public EmergencyToken sendEmergency() throws RemoteException 
  {
    EmergencyToken emtoken = new EmergencyToken(nodeName, nextNode.getName());
    System.out.println(nodeName + " TRASMETTE LA RISPOSTA DI EMERGENZA!!!!");
    return emtoken; 
  }//[m]sendEmergency 

  /**
  *Metodo per la trasmissione in multicast di un messaggio all'interno di un'architettura TokenRing 
  *Tre valori booleani utilizzati: 
  *flag : resta a true fino a che la trasmissione non si e' conclusa con successo 
  *intflag2 : sempre a true tranne quando si riceve una versione vecchia di un messaggio, in tal caso aspetto la nuova versione 
  *trasmessa; situazione che si verifica nel momento in cui scade il timer poco prima di ricevere il messaggio che era stato precedentemente inviato
  *immediateRetx : settato a true nel momento in cui non ho ricevuto il messaggio di ritorno allo scadere del timer; in tal caso 
  *non avendo ricevuto nulla salto i check per il controllo di versione 
  *@param mex messaggio da trasmettere in multicast, tale messaggio viene poi convertito in un array di byte dal costruttore di MulticastMessage
  *@param dest insieme dei nodi di destinazione all'interno del ring attuale a cui inviare il multicast message 
  *@param nodesNumber numero totale di nodi attualmente iscritti al registry RMI e quindi presenti all'interno del ring 
  */
  public void mSend(String mex, ArrayList<String> dest, int nodesNumber) throws RemoteException
  {
    //Conta il numero di messaggi che ho inviato, ritrasmetto un messaggio precedentemente inviato nel momento in cui 
    //scade il timer e non ho ancora ricevuto il messaggio precedente di ritorno  
    int counter = 0; 
    //Il messaggio mex viene codificato in un array di byte ed inviato ai nodi presenti in dest, la codifica viene eseguita 
    //direttamente dal costruttore della classe MulticastMessage  
    MulticastMessage mmex = new MulticastMessage(nodeName, dest, mex);
    //NOTA BENE: NON DEVO TRASMETTERE UN MULTICASTMESSAGE NEL RING MA UN ARRAY DI BYTE! DA IMPLEMENTARE UN METODO CHE ESEGUA LA CONVERSIONE 
    boolean flag = true; 
    boolean intflag2 = true; 
    boolean immediateRetx = false; 
    byte[] byteMex = null; 
    long delay = 20000; 

    while(flag && thisNodeAlive)
    {
      if(intflag2)
      {
        synchronized(sendContainer)
        {
          counter++; 
          mmex.incrementVersion(); 
          sendContainer.addMMessage(mmex); 
          sendContainer.notify(); 
        }
      }
      
      boolean skipDelay = false; 
      /*
      Se un nodo all'interno del ring riceve il messaggio che avevo trasmesso e poi cade con il messaggio nessuno nel ring si 
      accorge di nulla; mi accorgo infatti della caduta di un nodo solo nel momento in cui trasmetto un token al nodo successivo 
      e tale nodo non lo legge, oppure quando invio un messaggio al nodo successivo e tale nodo non lo riceve! 
      In tal caso il ring rischia di andare in stallo. 
      La soluzione proposta di seguito e' questa: nel momento in cui invio un messaggio tale messaggio contiene al suo interno un numero
      di versione, nel momento in cui trasmetto faccio partire un timer pari a (delay)*(numero di nodi nel ring), allo scadere del timer 
      ritrasmetto il messaggio considerando il messaggio precedente come perso. 
      Il timer deve essere settato con cautela per due motivi: 
      1) Se viene settato ad un valore troppo basso ritrasmetto sistematicamente prima di avere ricevuto il messaggio precedente 
      2) Se viene settato ad un valore troppo alto noto un effetto di eccessivo rallentamento della rete 

      Nel momento in cui ricevo un messaggio controllo se il numero di versione del messaggio coincide con il numero del contatore: 
      - Se il numero di versione del messaggio < numero del contatore allora scarto il messaggio ricevuto in quanto significa che ho 
      trasmesso un secondo messaggio prima di aver ricevuto il primo 
      - Se il numero di versione del messaggio coincide con il valore del contatore allora devo controllare che la lista di destinatari sia
      vuota, in caso contrario significherebbe che vi sono alcuni nodi che non hanno ricevuto il messaggio e di conseguenza tale messaggio 
      deve essere ritrasmesso   
      */
      //Al piu' attendo per (delay * nodesNumber)
      for(int i = 0; i < nodesNumber; i++)
      {
        synchronized(msendContainer)
        {
          //Se ho ricevuto di ritorno il messaggio precedentemente inviato...
          if(!msendContainer.getMulticastBox().isEmpty())
          {
            i = (nodesNumber-1); //esco dal ciclo for 
            skipDelay = true; //non aspetto piu' nulla
          }
        }
        //Devo rilasciare per forza il lock su msendContainer prima di eseguire lo sleep per 8 secondi 
        //altrimenti non potrebbero inserire il messaggio di ritorno in msendContainer e al successivo check 
        //lo troverei nuovamente vuoto come prima!  
        if(!skipDelay) //Non ho ancora ricevuto il messaggio di ritorno...aspetto...
        {
          try
          {
            Thread.currentThread().sleep(delay); 
          }
          catch(InterruptedException ie)
          {
            ie.printStackTrace(); 
          }
        }
      }

      //Timer scaduto!
      synchronized(msendContainer)
      {
        //Non ho ancora ricevuto il messaggio di ritorno allo scadere del timer 
        if(msendContainer.getMulticastBox().isEmpty() && thisNodeAlive)
        {
          //Devo ritrasmettere in quanto un nodo del ring potrebbe aver ricevuto un messaggio e poi essere caduto...
          flag = true; 
          intflag2 = true; //Invio un messaggio con la versione incrementata di uno 
          immediateRetx = true; //Ritrasmetto subito: non ho ricevuto il messaggio quindi non posso eseguire il check sulla versione del messaggio
          System.out.println(nodeName + " ritrasmette il messaggio!!!"); 
        }
        else if(thisNodeAlive) //Ho ricevuto il messaggio di ritorno 
        {
          immediateRetx = false; 
        }
      }
        
      MulticastMessage receivemex = null; 

      if((!immediateRetx) && thisNodeAlive)
      {
        synchronized(msendContainer)
        {
          receivemex = msendContainer.removeMMessage();           
        }

        //Il messaggio che ho ricevuto e' l'ultimo che avevo trasmesso...
        if(receivemex.getVersion() == counter)
        {
          if(receivemex.getDestination().isEmpty()) //Termino, messaggio arrivato a tutti gli interessati 
          {
            flag = false; 
            System.out.println("Messaggio inviato da " + nodeName + " arrivato a tutti gli interessati!!!"); 
          }
          else
          {
            //Potrebbero essere rimasti dei nomi di nodi all'interno della lista che non sono stati tolti in quanto 
            //sono caduti all'interno del ring prima di ricevere il messaggio, tali nodi vanno rimossi dalla lista e non 
            //considerati 
            List<ConnectionRequest> tempList = null; 
            ArrayList<String> remainingNodes = receivemex.getDestination(); 
            String[] aliveNodes = null; 
            ReentrantReadWriteLock lck = null; 

            lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
            
            lck.readLock().lock();   
              aliveNodes = myGroupRegistry.list();
            lck.readLock().unlock(); 
            
            boolean intflag = false; 
            for(int k = 0; k < aliveNodes.length; k++)
            {
              if(remainingNodes.contains(aliveNodes[k])) // Se esiste un nodo che non e' caduto e che non ha ricevuto il messaggio... 
              {
                intflag = true; 
                break; // ...devo ritrasmettere!!
              }
            }

            if(!intflag)
            {
              flag = false; // Ho terminato la trasmissione in multicast con successo 
            }
            else  
            {
              flag = true; // intflag e' a true --> ritrasmetto
              intflag2 = true; // Trasmetto un nuovo messaggio con la versione incrementata di uno 
            }
          }
        }
        else // Attendo l'ultimo messaggio che avevo trasmesso senza inviarne un altro! 
        {
          flag = true; // La trasmissione non si e' ancora conclusa con successo 
          intflag2 = false; // Non trasmetto un nuovo messaggio 
        }
      }
    }//endwhile
  }//[m]mSend


  /**
  *Thread principale della classe Node. Operazioni che vengono svolte da tale Thread sono, nell'ordine: 
  * 1-Attendo che venga inserito il token in internalContainer da parte del Thread TokenReceiver
  * 2-Segno il mio nome in tokenOwners dichiarando di essere il possessore attuale del token nel gruppo a cui sono stato assegnato 
  * 3-Controllo se vi sono nodi in coda in attesa di essere inseriti nel gruppo in cui sono, in caso di esito positivo ne prelevo 
  * uno dalla coda e lo inserisco dopo il nodo attuale(guardare metodo queueMonitor!)
  * 4-In modo probabilistico(scelta che puo' essere modificata, messa giusto per facilitare il testing della classe) decido se inviare un 
  * messaggio in multicast 
  * 5-Nel momento in cui l'invio del messaggio va a buon fine esco dalla sezione critica e invio il token al nodo successivo, controllando 
  * la corretta ricezione di quest'ultimo: nel caso in cui non venisse ricevuto dichiaro lo stato di emergenza 
  */
  public void run()
  {
    while(thisNodeAlive)
    {
      Token tk = null;  
      try
      {
        synchronized(internalContainer) 
        {
          //L'internalContainer deve essere settato a read dal thread TokenReceiver... 
          if(!internalContainer.isRead())
          {
            //Attendo fino a che non viene inserito il token in internalContainer 
            internalContainer.wait(); 
          }
          //Al risveglio... 
          tk = internalContainer.getList().remove(0);
          System.out.println(nodeName + " ha ricevuto un token !!!!"); 
          internalContainer.setValue(false); 
         
          //Mi segno come possessore attuale del token 
          synchronized(tokenOwners)
          {
            tokenOwners.put(Integer.valueOf(myGroup.hashCode()), nodeName);
          }
        }

        if(thisNodeAlive)
        {
          /*
          >>>>ACCESSO ALLA SEZIONE CRITICA, HO IO IL TOKEN E POSSO DECIDERE CHE COSA FARE, COME AD ESEMPIO UN MULTICAST SEND<<<<< 
          Nel momento in cui mi viene passato il token controllo se vi sono nodi in attesa di essere aggiunti al token ring in cui sono
          e nel caso in cui fossero presenti ne estraggo uno dalla coda e lo aggiungo dopo il nodo attuale nel ring a cui sono stato assegnato
          */
          queueMonitor(); 

          int value = randVal(0,2); 
          List<ConnectionRequest> tempList = null; 
          ReentrantReadWriteLock lck = null; 
          ArrayList<String> dest = new ArrayList<String>(); 
          
          if(value == 1)
          {
            //Decido di trasmettere un messaggio in multicast 
            lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
            
            //Codice per il testing di mSend 
            int numberOfNodes = 0; 
            String[] res = null; 
            
            lck.readLock().lock();  
              res = myGroupRegistry.list();
            lck.readLock().unlock(); 

            if(res.length >= 3)
            {
              if(!(res[0].equals(nodeName)))
              {  
                dest.add(res[0]);
              }

              if(!(res[res.length-1].equals(nodeName)))
              {
                dest.add(res[res.length-1]);
              }

              if(!(res[(res.length-1)/2].equals(nodeName)))
              {
                dest.add(res[(res.length-1)/2]);  
              }
              numberOfNodes = res.length; 
            }

            if(numberOfNodes >= 3)
            {
              System.out.println(nodeName + " trasmette un messaggio a :" + res[0] + ", " + res[res.length-1] + ", " + res[(res.length-1)/2]);
              mSend("Ciao sono il nodo " + nodeName, dest, numberOfNodes);
            }
          }

          Thread.currentThread().sleep((long)3000); 
          
          if(thisNodeAlive)
          {
            boolean sendOk = false; 

            //Continuo a ritrasmettere il token fino a quando l'invio non va a buon fine 
            //Nel momento in cui sendOk e' true allora il nodo successivo ha ricevuto con successo il token 
            while(!sendOk)
            {
              //Metodo sincronizzato con il container di nextNode 
              nextNode.sendToken(tk); 

              System.out.println(nodeName + " ha trasmesso un token a " + nextNode.getName() + " !!!!");
              
              //Second chance:
              //prima di dichiarare il nodo successivo come caduto eseguo due volte il controllo 
              //del container di nextNode per vedere se viene settato a read 
              for(int i = 0; i < 2; i++)
              {
                Thread.currentThread().sleep((long)3000);
                
                synchronized(nextNode.getContainer())
                {
                  if(nextNode.getContainer().isRead())
                  {
                    break; 
                  }
                } 
              }
              
              //Se dopo la second chance il container di nextNode non e' ancora stato letto 
              //dichiaro il nodo successivo caduto e invoco lo stato di emergenza 
              synchronized(nextNode.getContainer())
              {
                if(!nextNode.getContainer().isRead()) 
                {
                  //NODO SUCCESSIVO CADUTO!! --> chiamo l'emergency state!!!
                  System.out.println(nextNode.getName()+" CADUTO " + " !!!!"); 
                  System.out.println(nodeName + " chiama lo stato di emergenza!!!!");
                  emergencyState(); 
                } 
                else
                {
                  sendOk = true;
                }
              }
            }
          }
        }
      }
      catch(RemoteException re)
      {
        re.printStackTrace(); 
      } 
      catch(InterruptedException ie)
      {
        ie.printStackTrace(); 
      } 
    }//endwhile 
  }//[m]run    

  /**
  *Nel momento in cui un nodo si accorge che il nodo successivo nel ring e' caduto invoca l'emergencyState e  
  * trasmette un token di emergenza(EmergencyToken) a tutti i membri del gruppo a cui appartiene: solo in questa particolare situazione 
  * il nodo che si accorge che il nodo successivo e' caduto ha diritto ad accedere all'RMI registry del gruppo a cui appartiene riuscendo 
  * ad ottenere così i riferimenti ai nodi che fanno parte del gruppo. 
  * Ciascun nodo che riceve l'emergencyToken risponde comunicando il proprio nome ed il nome del nodo successivo all'interno del gruppo. 
  * L'algoritmo di elezione del nuovo nodo a cui il nodo attuale deve connettersi verra' spiegato nelle slides in fase di discussione del progetto. 
  */
  public void emergencyState() throws RemoteException 
  {
    List<ConnectionRequest> tempList = null; 
    ReentrantReadWriteLock lck = null; 
    String[] nodeList = null; 
    List<EmergencyToken> nodes = null; 
    boolean flag = true; 

    try
    {
      lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
      tempList = joinQueue.get(Integer.valueOf(myGroup.hashCode()));

      lck.readLock().lock(); 
        nodeList = myGroupRegistry.list(); 
      lck.readLock().unlock(); 

      //Se sono rimasto l'unico iscritto nel ring, caso in cui il ring e' formato da due soli nodi ed uno dei due cade 
      if(nodeList.length == 1) 
      {
        flag = false; 
      }
      else 
      {
        nodes = new ArrayList<EmergencyToken>(); 
        NodeInterface tmp = null; 

        for(int i = 0; i < nodeList.length; i++)
        {
          if(!(nodeList[i].equals(nodeName)))
          {
            lck.readLock().lock();   
              tmp = (NodeInterface)(myGroupRegistry.lookup(nodeList[i])); 
            lck.readLock().unlock(); 
                
            System.out.println(nodeName + " ha inviato un messaggio di emergenza a " + tmp.getName()); 
            nodes.add(tmp.sendEmergency());
          }
        }
      }

      if(flag)
      {
        ArrayList<LinkedList<String>> graphComponents = new ArrayList<LinkedList<String>>(); 
        LinkedList<String> list = new LinkedList<String>(); 
        LinkedList<String> linkedlist = null; 
        EmergencyToken emtok = nodes.remove(0);
        String from = emtok.getFrom(); 
        String to = emtok.getTo(); 
        list.addFirst(from);
        list.addLast(to); 
        graphComponents.add(list); 
        boolean flag2 = false; 

        while(!nodes.isEmpty())
        {  
          emtok = nodes.remove(0);
          from = emtok.getFrom(); 
          to = emtok.getTo();  
          flag2 = false; 
            
          for(LinkedList<String> lst : graphComponents)
          {
            if(lst.contains(from) || lst.contains(to)) //Non possono essere entrambi a true altrimenti avrei una coppia di nodi ripetuta nel ring
            {
              if(lst.contains(from))
              {
                lst.addLast(to); 
              }
              else 
              {
                lst.addFirst(from); 
              }

              flag2 = true; 
              break; 
            }
          }

          if(!flag2)
          {
            LinkedList<String> linked = new LinkedList<String>(); 
            linked.addFirst(from); 
            linked.addLast(to); 
            graphComponents.add(linked); 
          }
        }

        ArrayList<LinkedList<String>> graphComponents2 = new ArrayList<LinkedList<String>>(); 
        Iterator<LinkedList<String>> iterator = null; 
        LinkedList<String> firstLink = graphComponents.remove(0); 
        ArrayList<LinkedList<String>> graphComponentsHolder = null; 

        if(graphComponents.size() == 0) //Vi era solo firstLink all'interno 
        {
          graphComponents2.add(firstLink); 
        }
        else
        {
          while(!graphComponents.isEmpty())
          {
            iterator = graphComponents.iterator();
            graphComponentsHolder = new ArrayList<LinkedList<String>>(); 

            while(iterator.hasNext())
            {
              linkedlist = iterator.next(); 

              if(firstLink.getLast().equals(linkedlist.getFirst()))
              {
                graphComponentsHolder.add(linkedlist); 
                while(!linkedlist.isEmpty())
                {
                  firstLink.addLast(linkedlist.removeFirst()); 
                }
              }
              else if(firstLink.getFirst().equals(linkedlist.getLast())) 
              {
                graphComponentsHolder.add(linkedlist); 
                while(!linkedlist.isEmpty())
                {
                  firstLink.addFirst(linkedlist.removeLast()); 
                }
              }
            }
            graphComponents2.add(firstLink); 

            for(LinkedList<String> link : graphComponentsHolder)
            {
              graphComponents.remove(link); 
            }
            
            if(graphComponents.size() == 1) 
            {
              graphComponents2.add(graphComponents.remove(0)); 
            }
            else if(graphComponents.size() > 1)
            {
              firstLink = graphComponents.remove(0); 
            }
          }//endWhile
        }//endElse

        //A questo punto tutte le componenti connesse del grafo si trovano all'interno di graphComponents2 
        String newNextNode = null; 
        if(graphComponents2.size() == 1) //Unica componente connessa, caso in cui e' caduto un solo nodo nel ring 
        {
          newNextNode = graphComponents2.get(0).getFirst();
        } 
        else //Numero di componenti connesse >= 2, va considerata una delle componenti connesse che non contiene al suo interno il nodo attuale!!  
        {
          for(LinkedList<String> component : graphComponents2)
          {
            if(!component.contains(nodeName))
            {
              newNextNode = component.getFirst(); 
              break; 
            }
          }
        }

        lck.readLock().lock();  
          nextNode = (NodeInterface)(myGroupRegistry.lookup(newNextNode)); 
        lck.readLock().unlock(); 

        System.out.println(nodeName + " ha settato come nodo successivo nel ring " + nextNode.getName()); 
      }
      else //Sono rimasto l'unico iscritto al gruppo 
      {
        lck.readLock().lock();  
          nextNode = (NodeInterface)(myGroupRegistry.lookup(nodeName)); 
        lck.readLock().unlock();
      }
    }
    catch(RemoteException | NotBoundException re)
    {
      re.printStackTrace(); 
    }
  }//[m]emergencyState

//************************************************************************************************************************************                  
  /**
  * Nel momento in cui il nodo attuale riceve il token(ottenendo così accesso alla sezione critica) tale nodo controlla se vi sono richieste
  * di connessione pendenti in coda, se ci sono estrae un nodo dalla coda, lo inserisce dopo sè stesso all'interno del ring ed esegue il binding di 
  * tale nodo sul registry RMI, infine risveglia il nodo che era in coda tramite un notify sulla richiesta di connessione presentata da tale nodo. 
  */
  public void queueMonitor() throws RemoteException
  {
    ConnectionRequest conn = null; 
    List<ConnectionRequest> tempList = joinQueue.get(Integer.valueOf(myGroup.hashCode())); 
    ReentrantReadWriteLock lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
                
    synchronized(tempList)
    {
      if((tempList.size() != 0) && thisNodeAlive) //Qualcuno ha postato una richiesta di connessione al gruppo
      {
        conn = tempList.remove(0); 
        NodeInterface remNode = conn.getNode();    
        try
        {
          System.out.println(nodeName + " ha accolto la richiesta di connessione eseguita da " + remNode.getName());
          remNode.setNextNode(nextNode); 
          nextNode = remNode;

          lck.writeLock().lock();
            myGroupRegistry.rebind(remNode.getName(), remNode.getStub()); 
          lck.writeLock().unlock(); 
        }
        catch(RemoteException re)
        {
          re.printStackTrace(); 
        }
       
        //Risveglio il nodo che era in coda in attesa di essere inserito nel gruppo!!
        synchronized(conn)
        {
          //Se il nodo che voglio risvegliare non e' riuscito ad acquisire il lock sulla connectionRequest prima di me, 
          //settando il parametro isRead a true evito che tale nodo rimanga in wait perenne sulla richiesta di connessione che presenta... 
          conn.setToReceived();
          conn.notify(); 
        }
      }
    }
  }//[m]QueueMonitor 
               
//************************************************************************************************************************************

  /**
  *Thread che monitora periodicamente(ogni 2 secondi) container al fine di verificare se e' stato ricevuto 
  * un nuovo token dal nodo che precede il nodo attuale nel token ring. 
  * Nel momento in cui arriva il token vengono eseguite due azioni principali: 
  * 1-Rimuovo il token da container e setto a true il parametro isRead di container: in questo modo il nodo che mi precede e che mi ha 
  * trasmesso il token vede che ho ricevuto con successo il token e che quindi non sono caduto 
  * 2-Invio il token a internalContainer(che viene monitorato periodicamente dal Thread principale della classe), setto il parametro isRead
  * di internalContainer a true ed eseguo un notify sempre su internalContainer: se il thread principale della classe si perde il notify in quanto 
  * non ha ancora acquisito il lock su internalContainer non ci sono problemi in quanto non eseguira' un wait su internalContainer dal momento 
  * che isRead di internalContainer e' stato settato a true(guardare metodo run!)
  */
  private class TokenReceiver implements Runnable
  {
    public void run()
    {
      while(thisNodeAlive)
      {
        synchronized(container) 
        {
          if(!container.getList().isEmpty())
          {
            Token tok = container.getList().remove(0);
            container.setValue(true);  
                    
            synchronized(internalContainer)
            {
              try
              {
                sendToInternalContainer(tok);
              }
              catch(RemoteException re)
              {
                re.printStackTrace(); 
              }

              internalContainer.setValue(true); 
              internalContainer.notify();  
            }
          }
        }  
                  
        try
        {
          Thread.currentThread().sleep((long)2000); 
        }
        catch(InterruptedException ie)
        {
          ie.printStackTrace(); 
        }
      }//endwhile
    }//[m]run
  }//{c}TokenReceiver
     
//*********************************************************************************************************************************
  /**
  *Thread che periodicamente controlla che il possessore del token del gruppo/ring a cui appartengo 
  * sia ancora vivo, nel caso in cui mi accorgo che il token appartiene ad un nodo che e' caduto creo nuovamente il token 
  * e inizio a farlo circolare evitando così che il ring vada in uno stato di stallo. 
  * Se non ci fosse questo Thread, nel momento in cui un nodo riceve il token e cade, nessuno nel ring si accorgerebbe di nulla dal 
  * momento che la caduta di un nodo viene scoperta nel momento in cui passo un token o un messaggio a tale nodo! 
  */
  private class TokenOwnerMonitor implements Runnable 
  {
    public void run()
    { 
      String myGroupTokenKeeper = null; 
      boolean isThereATokenOwner = false; 
      
      while(thisNodeAlive)
      {
        try
        {
          String[] groupNodes = null; 
          ReentrantReadWriteLock lck = null; 
          /*
          Dal momento che l'operazione di generazione di un nuovo token in un ring e' abbastanza "delicata" eseguo un doppio 
          controllo sul parametro myGroupTokenKeeper(scelta di progetto); la circolazione di un unico token in ciascun ring differente
          e' di fondamentale importanza poiche' la circolazione di "doppioni" andrebbe a pregiudicare l'accesso in mutua esclusione 
          alla sezione critica 
          */ 
          for(int j = 0; j < 2; j++)
          {
            myGroupTokenKeeper = tokenOwners.get(Integer.valueOf(myGroup.hashCode()));
            lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
             
            synchronized(myGroupTokenKeeper)
            { 
              //Aggiorno il parametro myGroupTokenKeeper in quanto potrebbe essere stato cambiato dal nodo 
              //che ha acquisito il lock prima di me 
              myGroupTokenKeeper = tokenOwners.get(Integer.valueOf(myGroup.hashCode()));
              lck.readLock().lock();
                groupNodes = myGroupRegistry.list();
              lck.readLock().unlock(); 

              isThereATokenOwner = false; 
              for(int i = 0; i < groupNodes.length; i++)
              {
                if(groupNodes[i].equals(myGroupTokenKeeper)) //Il nodo possessore del token e' ancora nel gruppo e non e' caduto!! 
                {
                  isThereATokenOwner = true;  
                  break; 
                }
              }
          
              if((!isThereATokenOwner) && (j==1)) //Caduto il nodo che ha ricevuto il token... 
              {
                tokenOwners.put(Integer.valueOf(myGroup.hashCode()), nodeName);
                Token tk = new Token(); 
                sendToken(tk); //gia' sincronizzato 
                //Invio un nuovo token a me stesso perche' so di essere vivo!!
                //in questo modo anche se il nodo successivo nel ring e' caduto riesco ad accorgermene!! 
              }
              else if(isThereATokenOwner)
              {
                j = 1;  
              }
            } 
          Thread.currentThread().sleep((long)20000); 
          }
        Thread.currentThread().sleep((long)20000); 
        }
        catch(InterruptedException ie)
        {
          ie.printStackTrace(); 
        }
        catch(RemoteException re)
        {
          re.printStackTrace(); 
        }
      }//endWhile
    }//[m]run
  }//{c}TokenOwnerMonitor    

//***************************************************************************************************************************
  /**
  *Il MessageReceiver si sincronizza su receiveContainer ed esegue le seguenti operazioni in ordine:
  * 1 - Aspetta di ricevere un messaggio dal nodo che lo precede nel ring sotto forma di un array di byte
  * 2 - Converte il messaggio da un byte array ad un MulticastMessage in modo tale da poter poi sfruttare i metodi messi a disposizione 
  * da tale classe 
  * 3 - Se il nodo sorgente del messaggio coincide con il nodo attuale allora trasmette il messaggio(oggetto della classe MulticastMessage)
  * al MessageBox msendContainer, altrimenti rimuove il nodo attuale dalla lista di nodi di destinazione del messaggio, se presente, e trasmette
  * il messaggio sempre sotto forma di MulticastMessage al MessageBox sendContainer 
  */
  private class MessageReceiver implements Runnable
  {
    public void run()
    {
      while(thisNodeAlive)
      {
        MulticastMessage mrec2 = null; 
        byte[] mrec2Byte = null; 
        boolean ffg = false; 
        
        //receiveContainer contiene al suo interno un ArrayList<byte[]> 
        synchronized(receiveContainer)
        {   
          //Se mi e' arrivato un messaggio... 
          if(!(receiveContainer.getBox().isEmpty()))  
          {
            //Estraggo il messaggio che mi e' arrivato... 
            mrec2Byte = receiveContainer.removeMessage(); 
            //Converto il messaggio in un MulticastMessage in modo tale da poter sfruttare direttamente i metodi di tale classe 
            try
            {
              mrec2 = fromByteToMulticast(mrec2Byte); 
            }
            catch(RemoteException re)
            {
              re.printStackTrace(); 
            }
            receiveContainer.setValue(true);
            ffg = true; 
          } 
          
          //Ha senso eseguire i controlli sottostanti solo se ho ricevuto un messaggio e quindi solo se ffg e' a true 
          if(thisNodeAlive && ffg)
          {
            if(mrec2.getSource().equals(nodeName)) //Sorgente del messaggio e' il nodo attuale 
            {
              synchronized(msendContainer)
              {
                msendContainer.addMMessage(mrec2); 
              }
            }
            else //La sorgente del messaggio non coincide con il nodo attuale 
            {
              if(mrec2.getDestination().contains(nodeName))
              {
                mrec2.getDestination().remove(nodeName); 
                System.out.println("Rimosso il nodo attuale " + nodeName + " dal messaggio trasmesso da " + mrec2.getSource()); 
              }
              else 
              {
                System.out.println("Il messaggio non contiene il mio nome (" + nodeName + ")");         
              }
              
              //Ritardo che e' stato introdotto solo per testare il funzionamento della rete nel momento in cui un nodo 
              //riceve un messaggio e poi cade. 
              //Se non interessa il testing commentare questa parte di codice... 
              //*****************************************************
              try
              {
                Thread.currentThread().sleep(8000); 
              }
              catch(InterruptedException ie)
              {
                ie.printStackTrace(); 
              }
              //*****************************************************

              if(thisNodeAlive)
              {
                //Se la sorgente del messaggio coincide con un nodo che ora e' caduto rimuovo il messaggio e basta 
                List<ConnectionRequest> tempList = null; 
                ReentrantReadWriteLock lck = null; 
                boolean sourceAvailable = false; 
                String[] values = null; 
        
                lck = locks.get(Integer.valueOf(myGroup.hashCode())); 
                
                try
                {
                  lck.readLock().lock(); 
                    values = myGroupRegistry.list(); 
                  lck.readLock().unlock(); 
                }
                catch(RemoteException re)
                {
                  re.printStackTrace(); 
                }
                   
                for(int i = 0; i < values.length; i++)
                {
                  if(values[i].equals(mrec2.getSource())) //Il nodo sorgente del messaggio non e' caduto ed e' ancora presente nel ring...
                  {
                    sourceAvailable = true; 
                    break; 
                  }
                }
              
                if(sourceAvailable)
                {
                  synchronized(sendContainer)
                  {                
                    sendContainer.addMMessage(mrec2); 
                    sendContainer.setValue(true); 
                    sendContainer.notify(); 
                  }
                }
                else //Nodo sorgente del messaggio e' caduto, scarto il messaggio  
                {
                  //Scarto il messaggio senza risvegliare nessuno!!!
                  System.out.println("Scarto il messaggio poiche' " + mrec2.getSource() + " e' caduto!!!"); 
                }
              }
            }
          }
        }
      }
      try
      {
        Thread.currentThread().sleep((long)2000); 
      }
      catch(InterruptedException ie)
      {
        ie.printStackTrace(); 
      }
    }//[m]run
  }//{c}MessageReceiver

//***************************************************************************************************************************
  /**
  *Thread che esegue le seguenti operazioni nell'ordine: 
  * 1- Si sincronizza su sendContainer per vedere se e' arrivato un nuovo MulticastMessage 
  * 2- Converte il messaggio MulticastMessage arrivato in un array di byte 
  * 3- Trasmette il messaggio al nodo successivo del ring fino a quando quest'ultimo non lo riceve correttamente: nel caso in cui il 
  * nodo successivo fosse caduto, invoca lo stato di emergenza impostando un nuovo nextNode e ritrasmette il messaggio 
  */
  private class MessageSender implements Runnable
  {
    public void run()
    {
      boolean repeat = true;
      MulticastMessage ms = null; 
      MessageBox nextReceiver = null; 
      
      while(thisNodeAlive)
      {
        synchronized(sendContainer)
        {
          if(!sendContainer.isRead())
          {
            try
            {
              sendContainer.wait(); 
            }
            catch(InterruptedException ie)
            {
              ie.printStackTrace(); 
            }
          }
          //Risvegliato, c'e' un messaggio presente in sendContainer 
          ms = sendContainer.removeMMessage(); 
          sendContainer.setValue(false); 
          repeat = true; 
        }
        
        //Converto in un array di byte il messaggio ricevuto 
        byte[] msByte = ms.toByteArray(); 
        
        //Ripeto la trasmissione del messaggio al nodo successivo fino a che tale trasmissione non viene eseguita con successo 
        while(repeat) 
        { 
          try
          {
            nextNode.sendMessage(msByte); //Sincronizzazione interna al metodo sendMessage   

            //Controllo due volte per verificare se il messaggio e' stato ricevuto con successo o meno 
            for(int i = 0; i < 2; i++)
            {
              try
              {
                Thread.currentThread().sleep((long)3000); 
              }
              catch(InterruptedException ie)
              {
                ie.printStackTrace(); 
              }

              synchronized(nextNode.getReceiveContainer())
              {
                if(nextNode.getReceiveContainer().isRead())
                {
                  break; 
                }
              }
            }  
            
            //Se dopo il doppio controllo il receiveContainer del nodo successivo non e' ancora stato letto, dichiaro il nodo 
            //successivo del ring caduto 
            synchronized(nextNode.getReceiveContainer())
            {
              if(nextNode.getReceiveContainer().isRead())
              {
                repeat = false; 
              }
              else 
              {
                System.out.println(nodeName + " chiama lo stato di emergenza; " + nextNode.getName() + " CADUTO!!"); 
                emergencyState(); //Imposto il nuovo nextNode a cui connettermi nel ring e...
                repeat = true; //...ritrasmetto il messaggio 
              }
            }
          }
          catch(RemoteException re)
          {
            re.printStackTrace(); 
          } 
        }//endwhile(repeat)
      }//endwhile(thisNodeAlive)
    }//[m]run
  }//{c}MessageSender
}//{c}Node
