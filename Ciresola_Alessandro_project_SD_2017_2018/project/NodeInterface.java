package project; 

import java.rmi.*; 
import java.rmi.registry.*; 
import java.util.*; 

public interface NodeInterface extends Remote 
{
    Registry getRegistry() throws RemoteException; 
    void join() throws RemoteException; 
    void mSend(String mex, ArrayList<String> dest, int nodesNumber) throws RemoteException; 
    void sendToken(Token token) throws RemoteException; 
    void setNextNode(NodeInterface nextNode) throws RemoteException; 
    String getName() throws RemoteException; 
    void destroy() throws RemoteException; 
    Container getContainer() throws RemoteException; 
    EmergencyToken sendEmergency() throws RemoteException;
    void emergencyState() throws RemoteException;
    void queueMonitor() throws RemoteException; 
    int randVal(int minVal, int maxVal) throws RemoteException; 
    MessageBox getReceiveContainer() throws RemoteException;
    void sendMessage(byte[] sms) throws RemoteException; 
    Remote getStub() throws RemoteException;
    void sendToInternalContainer(Token token) throws RemoteException; 
    MulticastMessage fromByteToMulticast(byte[] byteMex) throws RemoteException; 
}
