package project; 

import java.util.*; 

public class Container implements java.io.Serializable 
{
  private List<Token> list; 
  private boolean isRead; 

  public Container()
  {
    list = new ArrayList<Token>(1); 
    isRead = false; 
  }

  /**
  *Metodo per impostare il parametro isRead al valore desiderato 
  *@param bool valore che si vuole assegnare al booleano isRead 
  */
  public void setValue(boolean bool)
  {
    isRead = bool;  
  }//[m]setValue

  /**
  *Metodo per restituire la lista di Token 
  *@return list lista di Token contenuta presente nel Container 
  */
  public List<Token> getList()
  {
    return list; 
  }//[m]getList
    
  /**
  *@return isRead: a true se il Container attuale e' stato letto, a false in caso contrario 
  */
  public boolean isRead()
  {
    return isRead; 
  }//[m]isRead
}//{c}Container 
